import argparse

# A function to read a parameter from a jO file
# Taken from https://github.com/retmas-dv/deftcore/blob/master/taskengine/taskdef.py#L289
# ignore_case is set to False by default unlike ProdSys
def _read_param_from_jo(jo, names, ignore_case=False):
    value = None
    if ignore_case:
        names = [name.lower() for name in names]
        jo = jo.lower()
    if any(p in jo for p in names):
        for line in jo.splitlines():
            if any(p in line for p in names):
                try:
                    if '#' in line:
                        line = line[:line.find('#')]
                    # The int conversion will throw an exception if nEventsPerJob is not a number
                    value = int(line.replace(' ', '').split('=')[-1])
                    break
                except Exception as ex:
                    pass
    return value

# A function to read a parameter from a jO file using a dictionary
# The difference with the previous one is that it follows
# See https://gitlab.cern.ch/atlas-physics/pmg/mcjoboptions/-/issues/98
def _read_param_from_jo_withDict(jo, type, param):
    locals = {type: argparse.Namespace()}
    for line in jo.splitlines():
        if "os.system" in line: continue # for security
        try:
            exec(line, {}, locals)
        except:
            pass
    return getattr(locals[type], param) if hasattr(locals[type], param) else None

### Steering ###
if __name__ == "__main__":
    import argparse
    # Argument parser
    parser = argparse.ArgumentParser()
    parser.add_argument("--jO", dest="jOFile", default=None)
    parser.add_argument("--parameter", dest="parameter", action="store", default=None)
    args = parser.parse_args()
    
    # Check if jO file has been defined
    if args.jOFile is None:
        raise Exception("ERROR: --jO argument not specified")
    
    # Read jO contents
    with open(args.jOFile, 'r') as jofile:
        job_options_file_content = jofile.read()
    
    # Extract different parameters
    if args.parameter == "nEventsPerJob":
        nEventsPerJob=_read_param_from_jo(job_options_file_content, ['evgenConfig.nEventsPerJob'])
        if not nEventsPerJob:
            nEventsPerJob=10000
        print(nEventsPerJob)
    elif args.parameter == "minevents":
        print(_read_param_from_jo(job_options_file_content, ['evgenConfig.minevents']))
