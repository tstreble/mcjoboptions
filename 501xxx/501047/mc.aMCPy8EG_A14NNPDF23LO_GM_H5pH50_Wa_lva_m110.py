import MadGraphControl.MadGraph_NNPDF30NLOnf4_Base_Fragment
from MadGraphControl.MadGraphUtils import *

nevents=10000*1.1

parameters = {'MASS':{'w+':8.038500e+01,'H':3.835360e+02,'H3z':3.195310e+02,'H3p':3.195310e+02,'H5z':1.100000e+02,'H5p':1.100000e+02,'H5pp':1.100000e+02},'POTENTIALPARAM':{'lam2':2.407230e-01,'lam3':2.840190e-02,'lam4':4.123390e-01,'lam5':-1.484630e+00,'M1coeff':8.449110e-02,'M2coeff':4.000000e+01},'SMINPUTS':{'aEWM1':1.322330e+02,'Gf':1.166380e-05,'aS':1.180000e-01},'VEV':{'tanth':1.000000e-04}}

process = """
        import model GM_UFO
        define p = g u c d s u~ c~ d~ s~
        generate p p > H5p H5z > w+ a a a
        add process p p > H5p~ H5z > w- a a a
        output -f
        """

if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = runArgs.ecmEnergy / 2.
else: 
    raise RuntimeError("No center of mass energy found.")
    
runName='run_01'

settings = {'nevents':nevents,'cut_decays':'T','pta':20.0,'ptl':20.0,'ptj':10.0,'etal':2.52,'etaa':2.52}

process_dir = new_process(process)

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings)
modify_param_card(process_dir=process_dir,params=parameters)

print_cards()

generate(process_dir=process_dir,runArgs=runArgs)
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")


evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.contact = ["Utsav Patel <utsav.mukesh.patel@cern.ch>"]
evgenConfig.description = 'MadGraph_GM_Wa_lva'
evgenConfig.keywords+=['BSM','exotic','BSMHiggs','leptonic']
