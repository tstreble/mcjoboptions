include ( 'MadGraphControl/SUSY_SimplifiedModel_PreInclude.py' )

# Set masses based on physics short
from MadGraphControl.MadGraphUtilsHelpers import get_physics_short
phys_short = get_physics_short()

masses['1000021'] = float(phys_short.split('_')[4]) #go
masses['1000022'] = float(phys_short.split('_')[5]) #N1
masses['1000023'] = float(phys_short.split('_')[5]) #N2
masses['1000024'] = float(phys_short.split('_')[5]) #C1

process = '''
import model RPVMSSM_UFO
define susysq = ul ur dl dr cl cr sl sr t1 t2 b1 b2
define susysq~ = ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~ t1~ t2~ b1~ b2~
generate    p p > go go     $ susysq susysq~ @1
add process p p > go go j   $ susysq susysq~ @2
add process p p > go go j j $ susysq susysq~ @3
'''


# Set up the decays 
decays['1000021'] = """DECAY   1000021     1.75737029E+01   # gluino decays
#          BR         NDA      ID1       ID2
    5.00000000E-01    3     1000022        6      -6   # BR(~g -> ~chi_10 t tb)
    2.00000000E-01    3     1000024        5      -6   # BR(~g -> ~chi_1+ b tb)
    2.00000000E-01    3    -1000024       -5       6   # BR(~g -> ~chi_1- bb t)
    1.00000000E-01    3     1000022        5      -5   # BR(~g -> ~chi_10 b bb)
"""
decays['1000022'] = """DECAY   1000022     1.00000000E+00   # neutralino1 decays
#          BR         NDA      ID1       ID2
    5.00000000E-01    3     3 6 5
    5.00000000E-01    3     -3 -6 -5
"""
decays['1000023'] = """DECAY   1000023     1.0E+00   # neutralino2 decays
#          BR         NDA      ID1       ID2
    5.00000000E-01    3     3 6 5
    5.00000000E-01    3     -3 -6 -5

"""
decays['1000024'] = """DECAY   1000024     9.43406075E-01   # chargino1+ decays
#          BR         NDA      ID1       ID2
    1.00000000E+00    3     -3 -5 -5
#     1.00000000E+00    3     -3 -5 -5
"""


# Set up a default event multiplier: 
evt_multiplier = 2

njets = 2

evgenConfig.contact  = ["oducu@cern.ch"]
evgenConfig.keywords += [ 'SUSY', 'RPV', 'gluino', 'Higgsino', 'simplifiedModel', 'neutralino']

evgenConfig.description = 'gluino pair production and decay to chargino/neutralino, which decay via RPV lampp332 coupling, m_gluino = %s GeV, m_N1 = %s GeV, m_C1 = %s GeV, m_N2 = %s GeV'%(masses['1000021'],masses['1000022'],masses['1000024'],masses['1000023'])

if '1L20' in phys_short.split('_')[-1]:
   evgenLog.info('1Lepton20 filter is applied')
   include ( 'GeneratorFilters/MultiElecMuTauFilter.py' )
   filtSeq.MultiElecMuTauFilter.MinPt  = 20000.
   filtSeq.MultiElecMuTauFilter.MaxEta = 2.8
   filtSeq.MultiElecMuTauFilter.NLeptons = 1
   filtSeq.MultiElecMuTauFilter.IncludeHadTaus = 0
   filtSeq.Expression = "MultiElecMuTauFilter"
   evt_multiplier = 5

include ( 'MadGraphControl/SUSY_SimplifiedModel_PostInclude.py' )

if njets>0:
   genSeq.Pythia8.Commands += ["Merging:Process = guess"]
   genSeq.Pythia8.UserHooks += ["JetMergingaMCatNLO"]