###############################
from MadGraphControl.MadGraphUtils import *

evgenConfig.nEventsPerJob = 2000
#nevents = runArgs.maxEvents*1.1 if runArgs.maxEvents>0 else 1.1*evgenConfig.nEventsPerJob

nevents = 2200

process = """
import model loop_sm
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define wpm = w+ w-
generate p p > w+ w- z [QCD]
output -f"""

#Fetch default NLO run_card.dat and set parameters
extras = { 'lhe_version'  :'2.0',
          'pdlabel'      :"'lhapdf'",
          'lhaid'        : 260000,
          'parton_shower':'PYTHIA8',
          'bwcutoff'     :'1000',
          'nevents'      :nevents
}

process_dir = new_process(process)
modify_run_card(runArgs=runArgs,process_dir=process_dir,settings=extras)

generate(process_dir=process_dir,runArgs=runArgs)

arrange_output(runArgs=runArgs,process_dir=process_dir,lhe_version=1)

# ############################
# # Shower JOs will go here
# #### Shower                    
evgenConfig.generators += ["MadGraph", "Pythia8"]
evgenConfig.description = 'MadGraph5_aMC@NLO_WWZ_lvlvqq'
evgenConfig.keywords+=['triboson']
evgenConfig.contact = ['Ada Farilla <ada.farilla@cern.ch>']

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_aMcAtNlo.py")

#--------------------------------------------------------------
# WWZ->lvlvqq at Pythia8
#--------------------------------------------------------------
genSeq.Pythia8.Commands += [ '24:onMode = off',#decay of W
                            '24:mMin = 2.0',
                            '24:onMode = off',
                            '24:onIfAny = 11 12 13 14 15 16',
                            '23:onMode = off',#decay of Z
                            '23:mMin = 2.0',
                            '23:onMode = off',
                            '23:onIfAny = 1 2 3 4 5']
###############################
