mH = 125
mfd2 = 55
mfd1 = 10
mZd = 15000
nGamma = 2
avgtau = 1000
decayMode = 'normal'
include("MadGraphControl_A14N23LO_FRVZdisplaced_wh.py")
evgenConfig.nEventsPerJob=2000
evgenConfig.keywords = ["exotic", "BSMHiggs", "BSM", "darkPhoton"]
