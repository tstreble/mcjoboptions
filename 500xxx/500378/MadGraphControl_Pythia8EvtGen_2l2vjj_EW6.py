import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

import os
### get MC job-options filename
FIRST_DIR = (os.environ['JOBOPTSEARCHPATH']).split(":")[0]
jofiles = [f for f in os.listdir(FIRST_DIR) if (f.startswith('mc') and f.endswith('.py'))]

runNum=-1
runNumStr=str(runArgs.jobConfig)[-8:-2]
if runNumStr.isdigit():
  runNum=int(runNumStr)


#---------------------------------------------------------------------------
# General Settings
#---------------------------------------------------------------------------
mode=1 # 0: single core; 1: cluster; 2: multicore
cluster_type="condor"
cluster_queue=None
njobs=14
if mode!=2: njobs=1

gridpack_mode=True
gridpack_dir='madevent/'

## For filters
safefactor = 1.1

#---------------------------------------------------------------------------
# Process types (proc_name should be specified in the top JO for each process)
#---------------------------------------------------------------------------
proc_names=["VBF2l2v_SBI", "VBF2l2v_B", "VBF2l2v_SBI5", "VBF2l2v_SBI10", "VBF2l2v_S"]

model=""
process=""
description=""
keywords=[]

masses={'25': '1.250000e+02'}        ## Higgs mass
decays={'25': 'DECAY  25 4.07e-03'}  ## Higgs width
extras_cuts = {} ## mass filters

# 2l2v, off-shell Z->ll
if proc_name and proc_name in proc_names:
    ## SBI
    if proc_name=="VBF2l2v_SBI":
        model="import model sm"
        process="generate p p > j j e+ e- vm vm~ QCD=0 QED=6\n add process p p > j j mu+ mu- ve ve~ QCD=0 QED=6"
        description='qq->jj2l2v sample, off-shell Higgs included, different lepton and neutrino flavors to exclude WW'
        keywords=['VBF','ZZ', '2lepton', 'neutrino', 'Higgs']

        safefactor=2.9  ## 1.1/0.395837
        extras_cuts={'mmnl':"20",
                     'mmll':"10",
        }
    ## B
    if proc_name=="VBF2l2v_B":
        model="import model sm"
        process="generate p p > j j e+ e- vm vm~ /h QCD=0 QED=6\n add process p p > j j mu+ mu- ve ve~ /h QCD=0 QED=6"
        description='qq->jj2l2v sample, off-shell Higgs excluded, different lepton and neutrino flavors to exclude WW'
        keywords=['VBF','ZZ', '2lepton', 'neutrino', 'Higgsless']

        safefactor=1.9 ## 1.1/0.602537
        extras_cuts={'mmnl':"20",
                     'mmll':"10",
        }
    ## SBI5
    if proc_name=="VBF2l2v_SBI5":
        model="import model sm\nimport model sm_5hw"
        process="generate p p > j j e+ e- vm vm~ QCD=0 QED=6\n add process p p > j j mu+ mu- ve ve~ QCD=0 QED=6"
        description='qq->jj2l2v sample, off-shell Higgs included, 5 times SM Higgs total width, different lepton and neutrino flavors to exclude WW'
        keywords=['VBF','ZZ', '2lepton', 'neutrino', 'BSMHiggs']

        safefactor=2.6 ## 1.1/0.439836
        masses={'25': '1.250000e+02'}        ## Higgs mass
        decays={'25': 'DECAY  25 2.035e-02'} ## Higgs width, x5
        extras_cuts={'mmnl':"20",
                     'mmll':"10",
        }
    ## SBI10
    if proc_name=="VBF2l2v_SBI10":
        model="import model sm\nimport model sm_10hw"
        process="generate p p > j j e+ e- vm vm~ QCD=0 QED=6\n add process p p > j j mu+ mu- ve ve~ QCD=0 QED=6"
        description='qq->jj2l2v sample, off-shell Higgs included, 10 times SM Higgs total width, different lepton and neutrino flavors to exclude WW'
        keywords=['VBF','ZZ', '2lepton', 'neutrino', 'BSMHiggs']

        safefactor=2.2 ## 1.1/0.501626
        masses={'25': '1.250000e+02'}        ## Higgs mass
        decays={'25': 'DECAY  25 4.07e-02'}  ## Higgs width, x10
        extras_cuts={'mmnl':"20",
                     'mmll':"10",
        }
    ## S
    if proc_name=="VBF2l2v_S":
        model="import model sm"
        process="generate p p > h > j j e+ e- vm vm~ QCD=0 QED=6\n add process p p > h > j j mu+ mu- ve ve~ QCD=0 QED=6"
        description='qq->h>jj2l2v sample, off-shell Higgs included, different lepton and neutrino flavors to exclude WW'
        keywords=['VBF','ZZ', '2lepton', 'neutrino', 'Higgs']

        safefactor=4.2 ## 1.1/0.268059
        extras_cuts={'mmnl':"20",
                     'mmll':"10",
        }
else:
    raise RuntimeError("proc_name not recognised in these jobOptions!")

#---------------------------------------------------------------------------
# MG5 Proc card, based on runNumber
#---------------------------------------------------------------------------
process_str="""
"""+model+"""
define p = g u c d s u~ c~ d~ s~
define j = g u c d s u~ c~ d~ s~
define l+ = e+ mu+
define l- = e- mu-
define vl = ve vm vt
define vl~ = ve~ vm~ vt~
#define za = z a
define za = z
"""+process+"""
output -f
"""

#----------------------------------------------------------------------------
# Random Seed
#----------------------------------------------------------------------------
randomSeed = 0
if hasattr(runArgs,'randomSeed'): randomSeed = runArgs.randomSeed

#----------------------------------------------------------------------------
# Beam energy
#----------------------------------------------------------------------------
if hasattr(runArgs,'ecmEnergy'):
    beamEnergy = int(runArgs.ecmEnergy) / 2.
else:
    raise RunTimeError("No center of mass energy found.")

#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------
if hasattr(runArgs,'maxEvents') and runArgs.maxEvents > 0:  nevents = int(runArgs.maxEvents*safefactor)
else: nevents = int(evgenConfig.nEventsPerJob*safefactor)

#---------------------------------------------------------------------------
# Number of Events
#---------------------------------------------------------------------------
skip_events=0
if hasattr(runArgs,'skipEvents'): skip_events=runArgs.skipEvents

#---------------------------------------------------------------------------
# MG5 Run Card
#---------------------------------------------------------------------------
extras = {
    'asrwgtflavor':"5",
    'lhe_version':"3.0",
    'ptj':"5",
    'ptb':"5",
    'pta':"0",
    'ptl':"3",
    'etaj':"6.5",
    'etab':"6.5",
    'etal':"3.0",
    'drjj':"0",
    'drll':"0",
    'draa':"0",
    'draj':"0",
    'drjl':"0",
    'dral':"0",
    'mmjj':"10",
    'mmbb':"10",
    'maxjetflavor':"5" ,
    'cut_decays'  :'T',
    'auto_ptj_mjj': 'F',
    'nevents'      : nevents,
}
extras.update(extras_cuts)

if not is_gen_from_gridpack():
  process_dir = new_process(process_str)
else:
  process_dir = MADGRAPH_GRIDPACK_LOCATION

modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=extras)

#---------------------------------------------------------------------------
# MG5 parameter Card
#---------------------------------------------------------------------------
params={}
params['MASS']=masses
params['DECAY']=decays
modify_param_card(process_dir=process_dir,params=params)

#---------------------------------------------------------------------------
# MG5 + Pythia8 setup and process (lhe) generation
#---------------------------------------------------------------------------
try:
    generate(process_dir=process_dir,grid_pack=gridpack_mode,runArgs=runArgs)
except RuntimeError as rte:
    for an_arg in rte.args:
        if 'Gridpack sucessfully created' in an_arg:
            print 'Handling exception and exiting'
            theApp.finalize()
            theApp.exit()
    print 'Unhandled exception - re-raising'
    raise rte


#--------------------------------------------------------------------------------------------------------------------
#--------------------------------------------------------------------------------------------------------------------

arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)

import os
if 'ATHENA_PROC_NUMBER' in os.environ:
    evgenLog.info('Noticed that you have run with an athena MP-like whole-node setup.  Will re-configure now to make sure that the remainder of the job runs serially.')
    njobs = os.environ.pop('ATHENA_PROC_NUMBER')
    # Try to modify the opts underfoot
    if not hasattr(opts,'nprocs'): mglog.warning('Did not see option!')
    else: opts.nprocs = 0
    print opts

#### Shower

include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")

# to turn on the dipole shower
genSeq.Pythia8.Commands += [
            "SpaceShower:dipoleRecoil = on"]

evgenConfig.generators = ["MadGraph", "Pythia8", "EvtGen"]
evgenConfig.description = description
evgenConfig.keywords+=keywords
evgenConfig.contact = ['Lailin Xu <lailin.xu@cern.ch>', 'Martina Javurkova <Martina.Pagacova@cern.ch>']
